package com.daffodil.bookmyshowapi.repository;

import com.daffodil.bookmyshowapi.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
