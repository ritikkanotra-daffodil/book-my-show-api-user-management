package com.daffodil.bookmyshowapi.repository;

import com.daffodil.bookmyshowapi.entity.Permission;
import com.daffodil.bookmyshowapi.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
}
