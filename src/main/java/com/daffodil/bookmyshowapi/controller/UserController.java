package com.daffodil.bookmyshowapi.controller;

import com.daffodil.bookmyshowapi.entity.User;
import com.daffodil.bookmyshowapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/")
    public User saveUser(@RequestBody User user) {
        return userService.saveUser(user);
    }

    @GetMapping("/{id}")
    public User fetchUserById(@PathVariable("id") Long userId) {
        return userService.fetchUserById(userId);

    }

    @DeleteMapping("/{id}")
    public String deleteUserById(@PathVariable("id") Long userId) {
        userService.deleteUserById(userId);
        return "User Deleted successfully!!";
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable("id") Long userId, @RequestBody User user) {

        return userService.updateUser(userId, user);
    }


}
