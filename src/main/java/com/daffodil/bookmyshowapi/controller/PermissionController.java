package com.daffodil.bookmyshowapi.controller;

import com.daffodil.bookmyshowapi.entity.Permission;
import com.daffodil.bookmyshowapi.entity.Resource;
import com.daffodil.bookmyshowapi.service.PermissionService;
import com.daffodil.bookmyshowapi.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/permissions")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    @PostMapping("/")
    public Permission savePermission(@RequestBody Permission permission) {
        return permissionService.savePermission(permission);
    }

    @GetMapping("/{id}")
    public Permission fetchPermissionById(@PathVariable("id") Long permissionId) {
        return permissionService.fetchPermissionById(permissionId);

    }

    @DeleteMapping("/{id}")
    public String deletePermissionById(@PathVariable("id") Long permissionId) {
        permissionService.deletePermissionById(permissionId);
        return "PermissionDeleted successfully!!";
    }

    @PutMapping("/{id}")
    public Permission updatePermission(@PathVariable("id") Long permissionId, @RequestBody Permission permission) {
        return permissionService.updatePermission(permissionId, permission);
    }
}
