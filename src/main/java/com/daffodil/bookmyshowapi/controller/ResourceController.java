package com.daffodil.bookmyshowapi.controller;

import com.daffodil.bookmyshowapi.entity.Resource;
import com.daffodil.bookmyshowapi.entity.Role;
import com.daffodil.bookmyshowapi.service.ResourceService;
import com.daffodil.bookmyshowapi.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/resources")
public class ResourceController {

    @Autowired
    private ResourceService resourceService;

    @PostMapping("/")
    public Resource saveResource(@RequestBody Resource resource) {
        return resourceService.saveResource(resource);
    }

    @GetMapping("/{id}")
    public Resource fetchResourceById(@PathVariable("id") Long resourceId) {
        return resourceService.fetchResourceById(resourceId);

    }

    @DeleteMapping("/{id}")
    public String deleteResourceById(@PathVariable("id") Long resourceId) {
        resourceService.deleteResourceById(resourceId);
        return "Resource Deleted successfully!!";
    }

    @PutMapping("/{id}")
    public Resource updateResource(@PathVariable("id") Long resourceId, @RequestBody Resource resource) {
        return resourceService.updateResource(resourceId, resource);
    }

}
