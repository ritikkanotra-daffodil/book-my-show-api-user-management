package com.daffodil.bookmyshowapi.controller;

import com.daffodil.bookmyshowapi.entity.Role;
import com.daffodil.bookmyshowapi.entity.User;
import com.daffodil.bookmyshowapi.service.RoleService;
import com.daffodil.bookmyshowapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/roles")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @PostMapping("/")
    public Role saveRole(@RequestBody Role role) {
        return roleService.saveRole(role);
    }

    @GetMapping("/{id}")
    public Role fetchRoleById(@PathVariable("id") Long roleId) {
        return roleService.fetchRoleById(roleId);

    }

    @DeleteMapping("/{id}")
    public String deleteRoleById(@PathVariable("id") Long roleId) {
        roleService.deleteRoleById(roleId);
        return "Role Deleted successfully!!";
    }

    @PutMapping("/{id}")
    public Role updateRole(@PathVariable("id") Long roleId, @RequestBody Role role) {
        return roleService.updateRole(roleId, role);
    }

}
