package com.daffodil.bookmyshowapi.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class RoleResourcePermission {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
//    private Role role;
//    private Resource resource;
//    private Permission permission;
    private String code;
}
