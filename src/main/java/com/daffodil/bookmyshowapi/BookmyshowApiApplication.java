package com.daffodil.bookmyshowapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookmyshowApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookmyshowApiApplication.class, args);
	}

}
