package com.daffodil.bookmyshowapi.service;

import com.daffodil.bookmyshowapi.entity.Role;
import com.daffodil.bookmyshowapi.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public Role saveRole(Role role) {
        return roleRepository.save(role);
    }

    public Role fetchRoleById(Long roleId) {
        return roleRepository.findById(roleId).get();
    }

    public void deleteRoleById(Long roleId) {
        roleRepository.deleteById(roleId);
    }

    public Role updateRole(Long roleId, Role role) {
        Optional<Role> found = roleRepository.findById(roleId);
        if (!found.isPresent()) {
            //  throw new EmployeeNotFoundException("Employee not found.");
        }
        Role dbRole = found.get();
        if (role.getName() != null && !role.getName().equals("")) {
            dbRole.setName(role.getName());
        }
        if (role.getDescription() != null && !role.getDescription().equals("")) {
            dbRole.setDescription(role.getDescription());
        }
        if (role.getCode() != null && !role.getCode().equals("")) {
            dbRole.setCode(role.getCode());
        }
        if (role.getIsActive() != null && !role.getIsActive()) {
            dbRole.setIsActive(role.getIsActive());
        }
        return roleRepository.save(dbRole);
    }

}
