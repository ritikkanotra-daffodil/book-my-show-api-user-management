package com.daffodil.bookmyshowapi.service;

import com.daffodil.bookmyshowapi.entity.Permission;
import com.daffodil.bookmyshowapi.entity.Resource;
import com.daffodil.bookmyshowapi.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PermissionService {

    @Autowired
    private PermissionRepository permissionRepository;



    public Permission savePermission(Permission permission) {
        return permissionRepository.save(permission);
    }

    public Permission fetchPermissionById(Long permissionId) {
        return permissionRepository.findById(permissionId).get();
    }
    public  void deletePermissionById(Long permissionId) {
        permissionRepository.deleteById(permissionId);

    }
    public Permission updatePermission(Long permissionId, Permission permission) {
        Optional<Permission> found = permissionRepository.findById(permissionId);
        if (!found.isPresent()) {
            //  throw new EmployeeNotFoundException("Employee not found.");
        }
        Permission dbPermission = found.get();
        if (permission.getName() != null && !permission.getName().equals("")) {
            dbPermission.setName(permission.getName());
        }
        if (permission.getDescription() != null && !permission.getDescription().equals("")) {
            dbPermission.setDescription(permission.getDescription());
        }
        if (permission.getCode() != null && !permission.getCode().equals("")) {
            dbPermission.setCode(permission.getCode());
        }

        return permissionRepository.save(dbPermission);
    }
}
