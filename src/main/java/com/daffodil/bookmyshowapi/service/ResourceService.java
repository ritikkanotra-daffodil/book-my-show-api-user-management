package com.daffodil.bookmyshowapi.service;

import com.daffodil.bookmyshowapi.entity.Resource;
import com.daffodil.bookmyshowapi.entity.Role;
import com.daffodil.bookmyshowapi.repository.ResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ResourceService {

    @Autowired
    private ResourceRepository resourceRepository;
    public Resource saveResource(Resource resource) {
        return resourceRepository.save(resource);
    }

    public Resource fetchResourceById(Long resourceId) {
        return resourceRepository.findById(resourceId).get();
    }

    public void deleteResourceById(Long resourceId) {
        resourceRepository.deleteById(resourceId);
    }

    public Resource updateResource(Long resourceId, Resource resource) {
            Optional<Resource> found = resourceRepository.findById(resourceId);
            if (!found.isPresent()) {
                //  throw new EmployeeNotFoundException("Employee not found.");
            }
            Resource dbResource = found.get();
            if (resource.getName() != null && !resource.getName().equals("")) {
                dbResource.setName(resource.getName());
            }
            if (resource.getDescription() != null && !resource.getDescription().equals("")) {
                dbResource.setDescription(resource.getDescription());
            }
            if (resource.getCode() != null && !resource.getCode().equals("")) {
                dbResource.setCode(resource.getCode());
            }

            return resourceRepository.save(dbResource);
    }
}
