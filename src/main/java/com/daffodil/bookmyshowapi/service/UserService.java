package com.daffodil.bookmyshowapi.service;

import com.daffodil.bookmyshowapi.entity.User;
import com.daffodil.bookmyshowapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public User fetchUserById(Long userId) {
        return userRepository.findById(userId).get();
    }

    public void deleteUserById(Long userId) {
        userRepository.deleteById(userId);
    }

    public User updateUser(Long userId, User user) {
        Optional<User> found = userRepository.findById(userId);
        if (!found.isPresent()) {
          //  throw new EmployeeNotFoundException("Employee not found.");
        }
        User dbUser = found.get();
        if (user.getFirstName() != null && !user.getFirstName().equals("")) {
            dbUser.setFirstName(user.getFirstName());
        }
        if (user.getLastName() != null && !user.getLastName().equals("")) {
            dbUser.setLastName(user.getLastName());
        }
        if (user.getEmail() != null && !user.getEmail().equals("")) {
            dbUser.setEmail(user.getEmail());
        }
        if (user.getMobile() != null && !user.getMobile().equals("")) {
            dbUser.setMobile(user.getMobile());
        }

        if (user.getGender() != null && !user.getGender().equals("")) {
            dbUser.setGender(user.getGender());
        }
        return userRepository.save(dbUser);
    }

}
